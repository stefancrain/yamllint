FROM python:3.8.0-alpine3.10
ENV PYTHON_UNBUFFERED 1
WORKDIR /data
COPY requirements.txt ./
RUN apk add --no-cache tini==0.18.0-r0 && \
    pip install --no-cache-dir -r requirements.txt && \
    pip uninstall pip -y && \
    rm requirements.txt && \
    adduser -D -u 1000 yamllint

USER yamllint
ENTRYPOINT ["/sbin/tini", "--", "yamllint" ]
CMD ["--help"]

LABEL org.label-schema.name="Yamllint" \
      org.label-schema.description="--" \
      org.label-schema.vendor="--" \
      org.label-schema.docker.schema-version="1.0" \
      org.label-schema.vcs-type="git"
